//
//  main.m
//  ColorTap
//
//  Created by Andy Leece on 8/21/14.
//  Copyright (c) 2014 Andy Leece. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CTAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([CTAppDelegate class]));
    }
}
