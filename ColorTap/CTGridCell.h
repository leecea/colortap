//
//  CTGridCell.h
//  ColorTap
//
//  Created by Andy Leece on 8/23/14.
//  Copyright (c) 2014 Andy Leece. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CTGridCell : NSObject

@property (nonatomic,strong) UIColor *color;
@property (nonatomic,assign) BOOL locked;

@end
