//
//  GameKitHelper.h
//  ColorTap
//
//  Created by Andy Leece on 1/7/15.
//  Copyright (c) 2015 Andy Leece. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <GameKit/GameKit.h>

static NSString *const presentAuthenticationVC = @"presentAuthenticationVC";

// Leader boards
static NSString *const PRACTICE_LEADER = @"SixByTen.PracticeLeaders";
static NSString *const FULL_LEADER =     @"SixByTen.FullGame";
// Achievements
static NSString *const ACHIEVE_GOLD =   @"SixByTen.Gold";
static NSString *const ACHIEVE_LEVEL2 = @"SixByTen.Level2";
static NSString *const ACHIEVE_LEVEL3 = @"SixByTen.Level3";
static NSString *const ACHIEVE_LEVEL4 = @"SixByTen.Level4";

@interface GameKitHelper : NSObject

@property (nonatomic, strong) UIViewController *authenticationVC;

+ (GameKitHelper *)sharedInstance;
- (void)authenticateLocalPlayer;
- (void)reportAchievementWithIdentifier:(NSString *)name;
- (void)reportScore:(NSInteger)score forLeaderboard:(NSString *)leaderboardIdentifier;

@end
