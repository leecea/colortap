//
//  CTGameGridVC.m
//  ColorTap
//
//  Created by Andy Leece on 8/22/14.
//  Copyright (c) 2014 Andy Leece. All rights reserved.
//

#import "CTGameGridVC.h"
#import "CTGridModel.h"
#import "CTMainModel.h"
#import "SoundHelper.h"

static const float MIN_GAME_TIME = 10;

static CGFloat const animationRotation = (M_PI * (5.0) / 180.0);    //the amount of rotation for bonus 'jiggle'

static NSString *const GAME_COMPLETE_MSG = @"Game Complete!";
static NSString *const GAME_OVER_TIMEOUT_MSG = @"Too slow!";

@interface CTGameGridVC ()

@property (nonatomic, strong) CTGridModel *gridModel;
@property (nonatomic, assign) CGSize cellSize;
@property (nonatomic, strong) UIImageView *bonusView;
@property (nonatomic, strong) NSTimer *gameTimer;
@property (nonatomic, assign) CGFloat gameTime;
@property (nonatomic, assign) CGFloat timePerLock;
@property (nonatomic, assign) NSInteger gamePenalty;
@property (nonatomic, assign) NSInteger gameBonus;
@property (nonatomic, assign) BOOL gameWon;
@property (nonatomic, assign) BOOL gameOver;
@property (nonatomic, weak) NSString *gameOverReason;
@property (nonatomic, assign) BOOL fullGameInProgress;

@end

@implementation CTGameGridVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.gridModel = [[CTGridModel alloc] init];
    self.gridModel.delegate = self;
}

- (void)viewWillLayoutSubviews
{
    // resize here because it is called after frame size is set to match the screen and before the collection view
    // calls sizeForItemAtIndexPath
    //
    CGRect newRect = [self.gridModel getNewFrameForCollectionView:self.collectionView];
    [self.collectionView setFrame:newRect];
    
    // get cell size
    //
    self.cellSize = CGSizeMake(self.collectionView.frame.size.width/GRID_WIDTH, self.collectionView.frame.size.height/GRID_HEIGHT);
}

#pragma mark - Game handling

- (void)startGameWithData:(NSDictionary *)levelData forFullGame:(BOOL)fullGame
{
    // init display of game stats
    //
    self.gameTime = 0.0;
    self.gamePenalty = 0;
    self.gameBonus = 0;
    [self.delegate displayGameTime:self.gameTime];
    [self.delegate displayPenalty:self.gamePenalty];
    [self.delegate displayBonus:self.gameBonus];
    
    // Use the levelData to set up game parameters
    //
    [self.gridModel loadModelWithData:levelData];
    self.bonusView = [[UIImageView alloc] initWithImage:levelData[LEVEL_IMAGE]];
    self.timePerLock = [levelData[LEVEL_TIME_PER_LOCK] floatValue];
    
    [self.gridModel resetAllCells];
    [self.collectionView reloadData];
    
    self.gameWon = NO;
    self.gameOver = NO;
    
    self.fullGameInProgress = fullGame;     // save full game flag
    
    self.gameTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(refreshTimeDisplay) userInfo:nil repeats:YES];
}

- (void)stopGame
{
    [self stopAnimatingBonusView:self.bonusView];
    [self.gameTimer invalidate];
    self.gameTimer = nil;
}

// Called by game model during cell touch handling, when it detects that game is won or lost.
// Flags are checked after touch is handled.

- (void)gameHasBeenWon
{
    self.gameWon = YES;
    self.gameOverReason = GAME_COMPLETE_MSG;
}

- (void)gameOverWithReason:(NSString *)reason
{
    self.gameOver = YES;
    self.gameOverReason = reason;
}

- (void)refreshTimeDisplay
{
    // update game time display
    //
    self.gameTime += self.gameTimer.timeInterval;
    [self.delegate displayGameTime:self.gameTime];
    
    if([self gameHasTimedOut]){
        
        [self gameOverWithReason:GAME_OVER_TIMEOUT_MSG];
        [self executeGameOverProcessing];
    }
}

- (BOOL)gameHasTimedOut
{
    return (self.gameTime > MIN_GAME_TIME && self.gameTime > (self.gridModel.lockCount * self.timePerLock));
}

- (void)executeGameOverProcessing
{
    [self stopGame];
    
    NSInteger finalScore = [self.gridModel gameScoreFromTime:self.gameTime andPenalty:self.gamePenalty andBonus:self.gameBonus];
    NSString *reason = (self.gameWon ? GAME_COMPLETE_MSG : self.gameOverReason);
    
    if(self.fullGameInProgress){
        
        [self.delegate fullGameLevelOverWithScore:finalScore andContinueFlag:self.gameWon andReason:reason];
    }
    else {
        
        [self.delegate displayGameOverMessageWithScore:finalScore andReason:reason];
    }
}

#pragma mark - Collection View protocol methods

// set up collection and cells

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collection numberOfItemsInSection:(NSInteger)section
{
    return 60;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"GridCell" forIndexPath:indexPath];
        
    cell.backgroundColor = [self.gridModel colorForCellAtIndexPath:indexPath];
    cell.contentView.hidden = ![self.gridModel lockForCellAtIndexPath:indexPath];
    
    if(indexPath.row == self.gridModel.bonusRow){
        cell.backgroundView = self.bonusView;
        [[SoundHelper sharedInstance] playBonusAppearedSound];
        [self animateBonusView:cell.backgroundView];
    }
    else cell.backgroundView = nil;
    
    return cell;
}

#pragma mark - Animation of bonus view

- (void)animateBonusView:(UIView *)viewToAnimate
{
    CGAffineTransform leftWobble = CGAffineTransformMakeRotation(-1.0 * animationRotation);
    CGAffineTransform rightWobble = CGAffineTransformMakeRotation(animationRotation);
    
    viewToAnimate.transform = leftWobble;  // starting point
    
    [UIView animateWithDuration:0.1
                          delay:0
                        options:UIViewAnimationOptionAllowUserInteraction | UIViewAnimationOptionRepeat | UIViewAnimationOptionAutoreverse
                     animations:^{
                         viewToAnimate.transform = rightWobble;
                     }
                     completion:nil];
}

- (void)stopAnimatingBonusView:(UIView *)viewToAnimate
{
    [viewToAnimate.layer removeAllAnimations];
    viewToAnimate.transform = CGAffineTransformIdentity;
}

#pragma mark - Cell touch

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    [self handleCellTouch:[self.collectionView cellForItemAtIndexPath:indexPath] atIndexPath:indexPath];
}

- (void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath
{
    [self handleCellTouch:[self.collectionView cellForItemAtIndexPath:indexPath] atIndexPath:indexPath];
}

- (void)handleCellTouch:(UICollectionViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    // only process touches if the game is running
    //
    if([self.gameTimer isValid]){
        
        [self stopAnimatingBonusView:self.bonusView];
        
        NSInteger timeAdjustment = [self.gridModel cellWasTouched:[self.collectionView cellForItemAtIndexPath:indexPath] AtIndexPath:indexPath];
        
        [[SoundHelper sharedInstance] playKeySoundForTouchType:timeAdjustment];
        
        [self.gridModel gridWillBeReloaded];
        
        [self.collectionView reloadData];
        
        if(timeAdjustment < 0){
            self.gameBonus -= timeAdjustment;
            [self.delegate displayBonus:self.gameBonus];
        }
        if(timeAdjustment > 0){
            self.gamePenalty += timeAdjustment;
            [self.delegate displayPenalty:self.gamePenalty];
        }
        
        // check the flags that are set by the grid model during cell touch handling
        //
        if(self.gameWon || self.gameOver) [self executeGameOverProcessing];
    }
}

#pragma mark - Cell layout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return self.cellSize;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(0.0, 0.0, 0.0, 0.0);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end

