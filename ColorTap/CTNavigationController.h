//
//  CTNavigationController.h
//  ColorTap
//
//  Created by Andy Leece on 1/7/15.
//  Copyright (c) 2015 Andy Leece. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GameKitHelper.h"

@interface CTNavigationController : UINavigationController

@end
