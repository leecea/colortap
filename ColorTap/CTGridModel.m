//
//  CTGridModel.m
//  ColorTap
//
//  Created by Andy Leece on 8/22/14.
//  Copyright (c) 2014 Andy Leece. All rights reserved.
//
// ****************************************************
// MODEL FOR GAME GRID VC.  GRID INFO AND RULES
// ****************************************************

#import "CTGridModel.h"
#import "CTGridCell.h"
#import "CTMainModel.h"

static const int   NO_BONUS =   -1;       //value for bonus row when there is no bonus
static const float BASELINE_SCORE = 2000; //base from which we substract times etc to calculate the game score

static NSString *const GAME_OVER_PENALTY_MSG = @"Penalty touched!";
static NSString *const GAME_OVER_BONUS_MSG = @"Missed a bonus!";

@interface CTGridModel()

@property (nonatomic, strong) NSMutableArray    *cells;
@property (nonatomic, strong) NSArray           *gridColors;
@property (nonatomic, strong) UIColor           *colorForThisGame;
@property (nonatomic, assign) NSInteger         checkCellForGameColor;
@property (nonatomic, assign) BOOL              gridNeedsGameColor;
@property (nonatomic, assign) NSInteger         cellsAssignedRandomColor;
@property (nonatomic, strong) UIColor           *colorToBeAssigned;
@property (nonatomic, strong) NSDate            *gameStartTime;
@property (nonatomic, assign) NSInteger         bonusTrigger;
@property (nonatomic, assign) NSInteger         lastBonusCount;
@property (nonatomic, assign) NSInteger         numberOfUnlockedCellsWithGameColor;

// Game properties that can vary by level
@property (nonatomic, assign) NSInteger         bonusTime;
@property (nonatomic, assign) NSInteger         penaltyTime;
@property (nonatomic, assign) BOOL              bonusSuddenDeath;
@property (nonatomic, assign) BOOL              penaltySuddenDeath;
@property (nonatomic, assign) NSInteger         bonusTriggerMin;
@property (nonatomic, assign) NSInteger         bonusTriggerMax;
@property (nonatomic, strong) UIColor           *levelColor;

@end

@implementation CTGridModel

- (id)init
{
    self = [super init];
    
    // create grid colors
    //
    self.gridColors = [NSArray arrayWithObjects:GRID_COLOR(0), GRID_COLOR(1), GRID_COLOR(2), GRID_COLOR(3), nil];
    
    // set this game's color to nil
    self.colorForThisGame = nil;
    
    // init cells array
    //
    self.cells = [NSMutableArray arrayWithCapacity:(GRID_HEIGHT*GRID_WIDTH)];
    
    for(int i = 0; i < (GRID_WIDTH*GRID_HEIGHT); i++){
        
        CTGridCell *newCell = [[CTGridCell alloc] init];
        [self.cells addObject:newCell];
    }
    
    // grid is going to loaded for the first time
    //
    [self gridWillBeReloaded];
    
    return self;
}

- (void)loadModelWithData:(NSDictionary *)levelData
{
    self.penaltyTime = [levelData[LEVEL_PENALTY] integerValue];
    self.bonusTime = [levelData[LEVEL_BONUS] integerValue];
    
    self.bonusSuddenDeath = [levelData[LEVEL_BONUS_DEATH] boolValue];
    self.penaltySuddenDeath = [levelData[LEVEL_PENALTY_DEATH] boolValue];
    
    self.bonusTriggerMin = [levelData[LEVEL_BONUS_MIN] integerValue];
    self.bonusTriggerMax = [levelData[LEVEL_BONUS_MAX] integerValue];
    
    self.levelColor = levelData[LEVEL_COLOR];
}

// called after each touch, when the grid will be reloaded
//
- (void)gridWillBeReloaded
{
    // set up flags and counters that let us ensure a pseudo-random cell is assigned the game color
    // after every reload
    
    self.gridNeedsGameColor = YES;
    
    long cellsRemaining = [self.cells count] - self.lockCount;
    self.checkCellForGameColor = (cellsRemaining == 1 ? 0 : arc4random_uniform((int)cellsRemaining));
    
    self.cellsAssignedRandomColor = 0;
    self.bonusRow = NO_BONUS;
    
    // set all the colors for the soon to be refreshed grid
    //
    [self setAllColors];
    
    // see if a bonus will be available for this turn and set bonusRow if needed
    //
    [self setBonusRow];
}

// called when the game starts
//
- (void)resetAllCells
{
    for(int i = 0; i < [self.cells count]; i++){
        
        CTGridCell *cell = self.cells[i];
        cell.locked = NO;
        
        self.cells[i] = cell;
    }
    
    // Initialize all the bonus row data
    //
    self.lockCount = 0;
    self.lastBonusCount = 0;
    self.bonusTrigger = self.bonusTriggerMin;
    [self setBonusRow];
 
    // Set the color for this game from the level data.
    // If the level specifies NO_COLOR, set game color to nil so that color is chosen by player's first touch
    //
    self.colorForThisGame = [self.levelColor isEqual:NO_COLOR] ? nil : self.levelColor;
 
    [self setAllColors];
    
    self.gameStartTime = [NSDate date];
}

// called by grid VC to get color for cell at indexPath
//
- (UIColor *)colorForCellAtIndexPath:(NSIndexPath *)indexPath
{
    return ((CTGridCell *)self.cells[indexPath.row]).color;
}

// called internally, before each grid reload, to set colors for every cell
//
- (void)setAllColors
{
    CTGridCell *cell;
    
    self.numberOfUnlockedCellsWithGameColor = 0;
    
    for(int i = 0; i < [self.cells count]; i++){
    
        cell = self.cells[i];
        
        // leave locked cells alone
        //
        if(!cell.locked){
            
            if(self.gridNeedsGameColor && self.colorForThisGame && self.cellsAssignedRandomColor == self.checkCellForGameColor){
                
                // this will ensure there is always at least one cell with the current game color
                self.gridNeedsGameColor = NO;
                self.colorToBeAssigned = self.colorForThisGame;
                
            } else {
                
                // get random color
                self.cellsAssignedRandomColor += 1;
                self.colorToBeAssigned = self.gridColors[arc4random_uniform((int)[self.gridColors count])];
                
                // if it is the game color, set the flag to show that we already have the color
                if(self.colorToBeAssigned == self.colorForThisGame) self.gridNeedsGameColor = NO;
            }
            
            // store the cell's new color and keep track of the game color count for bonus calculation
            //
            cell.color = self.colorToBeAssigned;
            if(cell.color == self.colorForThisGame) self.numberOfUnlockedCellsWithGameColor += 1;
            self.cells[i] = cell;
        }
    }
}

// is this cell locked?
//
- (BOOL)lockForCellAtIndexPath:(NSIndexPath *)indexPath
{
    return ((CTGridCell *)self.cells[indexPath.row]).locked;
}

#pragma mark - Cell was touched

// Manage updates when a cell is touched and return any time adjustment due to the touch.
// Called by the VC.
//
// Check for game won or game over and call delegate to set appropriate flags.
//
- (NSInteger)cellWasTouched:(UICollectionViewCell *)cellFromGrid AtIndexPath:(NSIndexPath *)indexPath
{
    CTGridCell *cell = self.cells[indexPath.row];
    NSInteger timeAdjustment = 0;
    
    if(cell.locked){
        
        cell.locked = NO;
        self.lockCount -= 1;
        timeAdjustment = self.penaltyTime;
        
    } else {
        
        if(self.colorForThisGame && cellFromGrid.backgroundColor != self.colorForThisGame){
            
            timeAdjustment = self.penaltyTime;     // touched a cell of the wrong color
            
        }
        else {
            
            // if this is the first cell touched, set the game color
            if(!self.colorForThisGame) self.colorForThisGame = cellFromGrid.backgroundColor;
            
            cell.locked = YES;
            self.lockCount += 1;
            
            // see if this is the last cell, which means game is over
            if(self.lockCount == [self.cells count]) [self.delegate gameHasBeenWon];
            
            // did they touch a bonus cell
            if(indexPath.row == self.bonusRow){
                
                timeAdjustment = self.bonusTime;
            }
            else {            
                // is game over because they missed a bonus cell
                if(self.bonusSuddenDeath && self.bonusRow != NO_BONUS) [self.delegate gameOverWithReason:GAME_OVER_BONUS_MSG];
            }
        }
    }
    
    // is game over because they touched a penalty
    if(self.penaltySuddenDeath && timeAdjustment == self.penaltyTime) [self.delegate gameOverWithReason:GAME_OVER_PENALTY_MSG];
    
    self.cells[indexPath.row] = cell;
    
    return timeAdjustment;
}

// Check if a bonus is triggered and set row that will be bonus.
// Sets NO_BONUS if there is no bonus this turn
//
- (void)setBonusRow
{
    self.bonusRow = NO_BONUS;               // Intialize to no bonus
    if(self.bonusTrigger <= 0) return;      // If the bonusTrigger value is not set, there can't be a bonus
    
    // Bonus occurs when lock count equals last bonus count plus current bonus trigger count
    // But, no bonus on last cell.
    //
    if(self.lockCount == (self.lastBonusCount + self.bonusTrigger) && self.lockCount < (GRID_HEIGHT*GRID_WIDTH-1)){
        
        // update counters that will drive the next bonus calculation
        //
        self.lastBonusCount = self.lockCount;
        self.bonusTrigger = (self.bonusTrigger == self.bonusTriggerMax ? self.bonusTriggerMin : self.bonusTrigger + 1);
        
        // pick random number between 0 and number of cells eligible to be a bonus cell
        //
        NSInteger iRandNum = arc4random_uniform((int)self.numberOfUnlockedCellsWithGameColor);
        
        // find eligible bonus cells and set the bonus row based on random counter set above
        //
        CTGridCell *cell;
        for(int i = 0; i < [self.cells count]; i++){
            
            cell = self.cells[i];
            
            if(!cell.locked && cell.color == self.colorForThisGame){
                
                if(iRandNum == 0){
                    self.bonusRow = i;     // found the bonus cell
                    break;
                }

                iRandNum--;                // decrement counter and loop around
            }
        }
    }
}

// Calculate the game score.
// If the game is partially completed, adjust the score based on the proportion of cells that have been locked
//
- (NSInteger)gameScoreFromTime:(CGFloat)time andPenalty:(NSInteger)penalty andBonus:(NSInteger)bonus
{
    int adjustedBaseline = (int)BASELINE_SCORE;
    
    if(self.lockCount < [self.cells count]){
        
       float fractionOfGameComplete = (float)self.lockCount / (float)[self.cells count];
       adjustedBaseline = (int)(BASELINE_SCORE * fractionOfGameComplete);
    }
    
    int score = adjustedBaseline - (time + penalty - bonus)*10;
    
    return MAX(score, 0);            // don't return negative scores
}

// Get size of collection view so it always displays the grid of cells with equal height and width
//
- (CGRect)getNewFrameForCollectionView:(UICollectionView *)collection
{
    int max_h = collection.frame.size.height;
    int max_w = collection.frame.size.width;
    
    int cell_szH = (int)(max_h / GRID_HEIGHT);
    int cell_szW = (int)(max_w / GRID_WIDTH);
    
    int cell_sz = cell_szH < cell_szW ? cell_szH : cell_szW;
    
    CGRect newRect;
    newRect.origin = collection.frame.origin;
    newRect.size.height = cell_sz * GRID_HEIGHT;
    newRect.size.width = cell_sz * GRID_WIDTH;
    
    return newRect;
}

@end
