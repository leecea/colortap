//
//  CTOpeningScreenVC.h
//  ColorTap
//
//  Created by Andy Leece on 10/9/14.
//  Copyright (c) 2014 Andy Leece. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <iAd/iAd.h>

@interface CTOpeningScreenVC : UIViewController

@property (assign, nonatomic) NSInteger highScoreFullGame;
@property (assign, nonatomic) NSInteger highScorePractice;

- (void)saveHighScores;
- (void)saveFullGameUnlocked;

@end
