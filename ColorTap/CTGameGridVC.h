//
//  CTGameGridVC.h
//  ColorTap
//
//  Created by Andy Leece on 8/22/14.
//  Copyright (c) 2014 Andy Leece. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CTViewController.h"

@interface CTGameGridVC : UICollectionViewController <UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout>

@property (nonatomic, weak) CTViewController *delegate;

- (void)stopGame;
- (void)gameHasBeenWon;
- (void)gameOverWithReason:(NSString *)reason;
- (void)startGameWithData:(NSDictionary *)levelData forFullGame:(BOOL)fullGame;

@end
