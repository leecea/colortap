//
//  SoundHelper.h
//  ColorTap
//
//  Created by Andy Leece on 1/25/15.
//  Copyright (c) 2015 Andy Leece. All rights reserved.
//

#import <Foundation/Foundation.h>

static NSString *const SOUND_EFFECTS = @"SoundEffects";

@interface SoundHelper : NSObject

@property (nonatomic, assign) BOOL soundOn;

+ (SoundHelper *)sharedInstance;
- (void)playKeySoundForTouchType:(NSInteger)touchType;
- (void)playBonusAppearedSound;

@end
