//
//  CTGridModel.h
//  ColorTap
//
//  Created by Andy Leece on 8/22/14.
//  Copyright (c) 2014 Andy Leece. All rights reserved.
//
// ****************************************************
// MODEL FOR GAME GRID VC.  GRID INFO AND RULES
// ****************************************************

#import <Foundation/Foundation.h>
#import "CTGameGridVC.h"

static const int GRID_WIDTH =  10;
static const int GRID_HEIGHT = 6;

@interface CTGridModel : NSObject

@property (nonatomic, weak) CTGameGridVC  *delegate;
@property (nonatomic, assign) NSInteger    bonusRow;
@property (nonatomic, assign) NSInteger    lockCount;

- (CGRect)      getNewFrameForCollectionView:(UICollectionView *)collection;
- (UIColor *)   colorForCellAtIndexPath:(NSIndexPath *)indexPath;
- (BOOL)        lockForCellAtIndexPath:(NSIndexPath *)indexPath;
- (void)        resetAllCells;
- (NSInteger)   cellWasTouched:(UICollectionViewCell *)cell AtIndexPath:(NSIndexPath *)indexPath;
- (NSInteger)   gameScoreFromTime:(CGFloat)time andPenalty:(NSInteger)penalty andBonus:(NSInteger)bonus;
- (void)        gridWillBeReloaded;
- (void)        loadModelWithData:(NSDictionary *)levelData;

@end
