//
//  CTNavigationController.m
//  ColorTap
//
//  Created by Andy Leece on 1/7/15.
//  Copyright (c) 2015 Andy Leece. All rights reserved.
//

#import "CTNavigationController.h"

@interface CTNavigationController ()

@end

@implementation CTNavigationController

- (void)viewDidLoad {
    [super viewDidLoad];

    // Calls to handle Game Center authentication
    //
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showAuthenticationVC:) name:presentAuthenticationVC object:nil];
    [[GameKitHelper sharedInstance] authenticateLocalPlayer];
}

#pragma mark - Callback to handle authentication

- (void)showAuthenticationVC:(NSNotification *)notification
{
    GameKitHelper *gameKitHelper = [GameKitHelper sharedInstance];
    
    [self.topViewController presentViewController:gameKitHelper.authenticationVC animated:YES completion:nil];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
