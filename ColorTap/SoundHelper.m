//
//  SoundHelper.m
//  ColorTap
//
//  Created by Andy Leece on 1/25/15.
//  Copyright (c) 2015 Andy Leece. All rights reserved.
//

#import "SoundHelper.h"
#import <AudioToolbox/AudioToolbox.h>

NSURL *soundFileURL;
NSString *soundFilePath;

static NSString *const BONUS_APPEARS =      @"bonusAppears";
static NSString *const CLICK_BONUS =        @"clickBonus";
static NSString *const CLICK_PENALTY =      @"clickPenalty";
static NSString *const CLICK_AND_LOCK =     @"clickAndLock";

static NSString *const SOUND_TYPE =         @"wav";

SystemSoundID clickAndLock;
SystemSoundID clickPenalty;
SystemSoundID clickBonus;
SystemSoundID bonusAppears;

static SoundHelper *sharedSoundHelper = nil;

@implementation SoundHelper

+ (SoundHelper *)sharedInstance
{
    if(sharedSoundHelper == nil) sharedSoundHelper = [[self alloc] init];
    
    return sharedSoundHelper;
}

- (id)init
{
    self = [super init];
    
    // init sounds
    [self initSoundID:&clickAndLock fromFile:CLICK_AND_LOCK];
    [self initSoundID:&clickBonus fromFile:CLICK_BONUS];
    [self initSoundID:&clickPenalty fromFile:CLICK_PENALTY];
    [self initSoundID:&bonusAppears fromFile:BONUS_APPEARS];
    
    return self;
}
//
//- (BOOL)areSoundEffectsOn
//{
//    return self.soundOn;
//}
//
//- (void)turnSoundEffectsOn:(BOOL)flag
//{
//    self.soundOn = flag;
//}

- (void)initSoundID:(SystemSoundID *)soundId fromFile:(NSString *)soundFileName
{
    soundFilePath = [[NSBundle mainBundle] pathForResource:soundFileName ofType:SOUND_TYPE];
    soundFileURL = [NSURL fileURLWithPath:soundFilePath];
    AudioServicesCreateSystemSoundID((__bridge CFURLRef)soundFileURL, soundId);
}

- (void)playKeySoundForTouchType:(NSInteger)touchType
{
    if(!self.soundOn) return;
    
    SystemSoundID soundToPlay = (touchType == 0 ? clickAndLock : touchType > 0 ? clickPenalty : clickBonus);
    AudioServicesPlaySystemSound(soundToPlay);
}

- (void)playBonusAppearedSound
{
    if(!self.soundOn) return;
    
    AudioServicesPlaySystemSound(bonusAppears);
}

@end
