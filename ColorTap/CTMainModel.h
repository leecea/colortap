//
//  CTMainModel.h
//  ColorTap
//
//  Created by Andy Leece on 9/21/14.
//  Copyright (c) 2014 Andy Leece. All rights reserved.
//
// ****************************************************
// MODEL FOR OVERALL VC.  HIGH-LEVEL GAME INFO
// ****************************************************

#import <Foundation/Foundation.h>

// Game colors
//
#define NO_COLOR      [UIColor whiteColor]
#define GRID_COLOR(L) (L==0?[UIColor greenColor]:L==1?[UIColor redColor]:L==2?[UIColor blueColor]:L==3?[UIColor grayColor]:nil)

// Keys for level data
//
static NSString *const LEVEL_SCORE = @"Score";
static NSString *const LEVEL_BONUS = @"Bonus";
static NSString *const LEVEL_PENALTY = @"Penalty";
static NSString *const LEVEL_PENALTY_DEATH = @"PenaltyDeath";
static NSString *const LEVEL_BONUS_MIN = @"BonusMin";
static NSString *const LEVEL_BONUS_MAX = @"BonusMax";
static NSString *const LEVEL_BONUS_DEATH = @"BonusDeath";
static NSString *const LEVEL_IMAGE = @"Image";
static NSString *const LEVEL_COLOR = @"Color";
static NSString *const LEVEL_TIME_PER_LOCK = @"TimePerLock";

@interface CTMainModel : NSObject

- (void)setupPracticeLevelData;
- (void)setupFullGameLevelData;

- (NSDictionary *)dataForLevel:(NSInteger)gameLevel;
- (NSInteger)scoreForLevel:(NSInteger)gameLevel;
- (NSString *)descriptionForLevel:(NSInteger)gameLevel;

- (NSInteger)maxLevel;

- (NSInteger)levelForScore:(NSInteger)score;

@end
