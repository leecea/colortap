//
//  GameKitHelper.m
//  ColorTap
//
//  Created by Andy Leece on 1/7/15.
//  Copyright (c) 2015 Andy Leece. All rights reserved.
//

#import "GameKitHelper.h"

static GameKitHelper *sharedGameKitHelper = nil;

@interface GameKitHelper ()

@property (nonatomic, strong) NSError *lastError;
@property (nonatomic, assign) BOOL gameCenterEnabled;

@end

@implementation GameKitHelper

+ (GameKitHelper *)sharedInstance
{
    if(sharedGameKitHelper == nil) sharedGameKitHelper = [[self alloc] init];
    
    return sharedGameKitHelper;
}

- (id)init
{
    self = [super init];
    
    self.gameCenterEnabled = YES;   // assume game center enabled by default
    
    return self;
}

- (void)authenticateLocalPlayer
{
    GKLocalPlayer *localPlayer = [GKLocalPlayer localPlayer];
    
    localPlayer.authenticateHandler = ^(UIViewController *viewController, NSError *error){
        
        if(error) [self manageError:error];
        
        if(viewController != nil){
            
            self.authenticationVC = viewController;
            [[NSNotificationCenter defaultCenter] postNotificationName:presentAuthenticationVC object:self];
        }
        else self.gameCenterEnabled = [GKLocalPlayer localPlayer].authenticated;    // avoid ARC retain loop (do not use previously declared localPlayer property)
    };
}

- (void)manageError:(NSError *)error
{
    self.lastError = error;
    NSLog(@"GameKitHelper Error: %@",[[self.lastError userInfo] description]);
}

#pragma mark - Achievements

- (void)reportAchievementWithIdentifier:(NSString *)identifier
{
    if(self.gameCenterEnabled){
        
       [GKAchievement loadAchievementsWithCompletionHandler:^(NSArray *achievements, NSError *error) {
           
           GKAchievement *achievement;
           
           for(achievement in achievements){
               if([achievement.identifier isEqualToString:identifier]) return;    // achievement already exists so nothing to do
           }
           
           // didn't find this achievement so add it
           //
           achievement = [[GKAchievement alloc] initWithIdentifier:identifier];
           if(achievement){
               achievement.percentComplete = 100.0;
               [GKAchievement reportAchievements:@[achievement] withCompletionHandler:^(NSError *error) {
                   if(error) [self manageError:error];
               }];
           }
       }];

    }
    else NSLog(@"GameKitHelper: User not authenticated when trying to add achievement");
}

#pragma mark - Leaderboards

- (void)reportScore:(NSInteger)score forLeaderboard:(NSString *)leaderboardIdentifier
{
    if(self.gameCenterEnabled){
        
        GKScore *newScore = [[GKScore alloc] initWithLeaderboardIdentifier:leaderboardIdentifier];
        newScore.value = (int64_t)score;
        newScore.context = 0;
        
        [GKScore reportScores:@[newScore] withCompletionHandler:^(NSError *error) {
            if(error) [self manageError:error];
        }];
    }
    else NSLog(@"GameKitHelper: User not authenticated when updating high-score");
}

@end
