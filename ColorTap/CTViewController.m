//
//  CTViewController.m
//  ColorTap
//
//  Created by Andy Leece on 8/21/14.
//  Copyright (c) 2014 Andy Leece. All rights reserved.
//

#import "CTViewController.h"
#import "CTGameGridVC.h"
#import "CTMainModel.h"
#import "CTOpeningScreenVC.h"
#import <QuartzCore/QuartzCore.h>
#import "GameKitHelper.h"

#define HIGH_SCORE_PRACTICE       @"HighScorePractice"

@interface CTViewController () <UIAlertViewDelegate>

@property (nonatomic, weak) CTGameGridVC                *gridVC;
@property (weak, nonatomic) IBOutlet UITextField        *gameTime;
@property (weak, nonatomic) IBOutlet UITextField        *gamePenalty;
@property (weak, nonatomic) IBOutlet UITextField        *gameBonus;
@property (weak, nonatomic) IBOutlet UILabel            *gamePenaltyLabel;
@property (weak, nonatomic) IBOutlet UIBarButtonItem    *startButton;
@property (weak, nonatomic) IBOutlet UIBarButtonItem    *mainMenuButton;
@property (strong, nonatomic) IBOutlet UIImageView      *levelImage;
@property (nonatomic, strong) CTMainModel               *mainModel;
@property (nonatomic, assign) NSInteger                 currentLevel;
@property (nonatomic, strong) NSDictionary              *currentLevelData;
@property (nonatomic, assign) NSInteger                 cumulativeScore;

@end

@implementation CTViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //self.canDisplayBannerAds = YES;
    
    // init model
    self.mainModel = [[CTMainModel alloc] init];
    
    if(self.playingFullGame) [self.mainModel setupFullGameLevelData];
    else                     [self.mainModel setupPracticeLevelData];
    
    // Old code - this view was replaced by alerts.  But this is a good example of how to customize appearance of a view
    //self.resultsView.layer.cornerRadius = 10;
    //self.resultsView.layer.borderWidth = 3;
    //self.resultsView.layer.borderColor = [[UIColor orangeColor] CGColor];
    //self.resultsView.hidden = YES;
    
    if(self.playingFullGame){
        [self displayFullGameInfoMessage];
    }
    else {
        self.currentLevel = [self.mainModel levelForScore:self.delegate.highScorePractice];
        [self displayPracticeInfoMessage];
    }
    
    [self resetGameUsingFullGameFlag:self.playingFullGame];
}

- (void)resetGameUsingFullGameFlag:(BOOL)fullGameFlag
{
    if(fullGameFlag){
        self.currentLevel = 0;
        self.cumulativeScore = 0;
    }
    [self loadCurrentLevelData];
}

// called by the Start button's IBAction but also called to start the next level of a full game
//
- (void)startGame
{
    [self setBarButtonsForGameIsRunning:YES];
    
    [self.gridVC startGameWithData:self.currentLevelData forFullGame:self.playingFullGame];
}

#pragma mark - Handle button interactions

- (IBAction)mainMenuPressed:(id)sender {
    [self.gridVC stopGame];
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)startPressed:(id)sender {
    
    [self startGame];
}

- (IBAction)stopPressed:(id)sender {
    
    [self.gridVC stopGame];
    [self setBarButtonsForGameIsRunning:NO];
}

- (void)setBarButtonsForGameIsRunning:(BOOL)gameIsRunning
{
    self.startButton.enabled = !gameIsRunning;
    self.mainMenuButton.enabled = !gameIsRunning;
}

#pragma mark - Handle end of full game level

- (void)fullGameLevelOverWithScore:(NSInteger)score andContinueFlag:(BOOL)continueGame andReason:(NSString *)reason
{
    self.cumulativeScore += score;
    
    if(continueGame) [self reportLevelCompletedAchievement:self.currentLevel];   // current level was completed so see if that is a Game Center achievement
    
    if(continueGame && self.currentLevel < [self.mainModel maxLevel]){
        
        self.currentLevel++;
        [self loadCurrentLevelData];
        [self displayFullGameInfoForNextLevel:self.currentLevel withScore:self.cumulativeScore];
    }
    else {
        
        [self displayFullGameOverMessageWithScore:self.cumulativeScore andReason:reason];
        [self.gridVC stopGame];
        [self setBarButtonsForGameIsRunning:NO];
    }
}

- (void)reportLevelCompletedAchievement:(NSInteger)level
{
    if(level == 1) return;                  // completion of level 1 is not an achievement
    
    NSString *achievementId = (level == 2? ACHIEVE_LEVEL2 : level == 3 ? ACHIEVE_LEVEL3 : ACHIEVE_LEVEL4);
    
    [[GameKitHelper sharedInstance] reportAchievementWithIdentifier:achievementId];
}

#pragma mark - Manage New Current Level

- (void)loadCurrentLevelData
{
    self.currentLevelData = [self.mainModel dataForLevel:self.currentLevel];
    [self displayImageForCurrentLevel];
}

- (void)displayImageForCurrentLevel
{
    [self.levelImage setImage:self.currentLevelData[LEVEL_IMAGE]];
    self.levelImage.backgroundColor = self.currentLevelData[LEVEL_COLOR];
    [self.levelImage setNeedsDisplay];                      // seems to work without this, but I'm leaving it in
}

#pragma mark - Manage message display

- (void)alertWithMessage:(NSString *)message andTitle:(NSString *)title andButtonLabel:(NSString *)buttonLabel andCancelAllowed:(BOOL)cancelAllowed
{
    // Check for new Alert Controller and use it if it's available, otherwise use old alerting model (pre-iOS 8)
    //
    if([UIAlertController class]){
        
        UIAlertController *alertController = [UIAlertController
                                              alertControllerWithTitle:title
                                              message:message
                                              preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *cancelAction = [UIAlertAction
                                       actionWithTitle:@"Cancel"
                                       style:UIAlertActionStyleCancel
                                       handler:^(UIAlertAction *action){}];  // No action required for cancel
        
        NSInteger startButtonIndex = 1;
        UIAlertAction *startAction = [UIAlertAction
                                   actionWithTitle:buttonLabel
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction *action)
                                   {
                                       [self alertView:nil didDismissWithButtonIndex:startButtonIndex];   // Re-use UIAlertViewDelegate method
                                   }];
        
        if(cancelAllowed) [alertController addAction:cancelAction];
        [alertController addAction:startAction];
        
        [self presentViewController:alertController animated:YES completion:nil];
    }
    else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
                                                        message:message
                                                       delegate:self
                                              cancelButtonTitle:(cancelAllowed ? @"Cancel" : nil)
                                              otherButtonTitles:buttonLabel,nil];
        [alert show];
    }
    
    [self setBarButtonsForGameIsRunning:NO];
}

// Method called when the alert is dismissed. Cancel button must always be at index 0.
//
- (void)alertView:(id)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if(buttonIndex > 0)[self startGame];
}

#pragma mark - Manage message requests

- (void)displayPracticeInfoMessage
{
    NSString *alertTitle = @"Practice Game Stats";
    NSString *alertButtonLabel = @"Start Game";
    NSString *message = [NSString stringWithFormat:@"High Score: %d",(int)self.delegate.highScorePractice];
        
    NSString *nextLine = (self.currentLevel == [self.mainModel maxLevel] ? @"\n\nAt highest level!" : [NSString stringWithFormat:@"\n\nNext level: %d",(int)[self.mainModel scoreForLevel:self.currentLevel+1]]);
        
    message = [message stringByAppendingString:nextLine];
    
    [self alertWithMessage:message andTitle:alertTitle andButtonLabel:alertButtonLabel andCancelAllowed:YES];
}

- (void)displayFullGameInfoMessage
{
    NSString *alertTitle = @"Full Game Stats";
    NSString *alertButtonLabel = @"Start Game";
    NSString *message = [NSString stringWithFormat:@"High Score: %d\n",(int)self.delegate.highScoreFullGame];
    
    [self alertWithMessage:message andTitle:alertTitle andButtonLabel:alertButtonLabel andCancelAllowed:YES];
}

- (void)displayFullGameInfoForNextLevel:(NSInteger)level withScore:(NSInteger)score
{
    NSString *alertTitle = [NSString stringWithFormat:@"Current Score: %ld",(long)self.cumulativeScore];
    NSString *alertButtonLabel = @"Start Next Level";
    NSString *message = [NSString stringWithFormat:@"Next Level\n%@\n", [self.mainModel descriptionForLevel:self.currentLevel]];
    
    [self alertWithMessage:message andTitle:alertTitle andButtonLabel:alertButtonLabel andCancelAllowed:NO];
}

- (void)displayGameOverMessageWithScore:(NSInteger)score andReason:(NSString *)reason
{
    NSString *alertTitle = @"Game Over";
    NSString *alertButtonLabel = @"New Game";
    NSString *message = [NSString stringWithFormat:@"%@\nScore: %d",reason,(int)score];
    NSString *nextLine;
    
    if(score > self.delegate.highScorePractice){
        
        self.delegate.highScorePractice = score;
        [self.delegate saveHighScores];
        [[GameKitHelper sharedInstance] reportScore:score forLeaderboard:PRACTICE_LEADER];
        
        NSInteger oldLevel = self.currentLevel;
        self.currentLevel = [self.mainModel levelForScore:score];
        
        if(oldLevel < self.currentLevel){
            
            nextLine = @"\n\nYou reached the next level!";
            
            [self loadCurrentLevelData];
            
            if(self.currentLevel == [self.mainModel maxLevel]){
                [self.delegate saveFullGameUnlocked];
                [[GameKitHelper sharedInstance] reportAchievementWithIdentifier:ACHIEVE_GOLD];
            }
        }
        else nextLine = @"\n\nThat's a new high score!";
        
        message = [message stringByAppendingString:nextLine];
    }
    else {
        message = [message stringByAppendingString:[NSString stringWithFormat:@"\n\nYour best score: %d",(int)self.delegate.highScorePractice]];
    }
    
    [self alertWithMessage:message andTitle:alertTitle andButtonLabel:alertButtonLabel andCancelAllowed:YES];
    [self resetGameUsingFullGameFlag:self.playingFullGame];
}

- (void)displayFullGameOverMessageWithScore:(NSInteger)score andReason:(NSString *)reason
{
    NSString *alertTitle = @"Game Over";
    NSString *alertButtonLabel = @"New Game";
    NSString *message = [NSString stringWithFormat:@"%@\nScore: %d",reason,(int)score];
    NSString *nextLine;
    
    if(score > self.delegate.highScoreFullGame){
        
        self.delegate.highScoreFullGame = score;
        [self.delegate saveHighScores];
        [[GameKitHelper sharedInstance] reportScore:score forLeaderboard:FULL_LEADER];
        
        nextLine = @"\n\nThat's a new high score!";
        
        message = [message stringByAppendingString:nextLine];
    }
    else {
        message = [message stringByAppendingString:[NSString stringWithFormat:@"\n\nYour best score: %d",(int)self.delegate.highScoreFullGame]];
    }
    
    [self alertWithMessage:message andTitle:alertTitle andButtonLabel:alertButtonLabel andCancelAllowed:YES];
    [self resetGameUsingFullGameFlag:self.playingFullGame];
}

#pragma mark - Manage stats display

- (void)displayGameTime:(CGFloat)time
{
    self.gameTime.text = [NSString stringWithFormat:@"%.2f",time];
}

- (void)displayPenalty:(CGFloat)penalty
{
    self.gamePenalty.text = [NSString stringWithFormat:@"%.2f",penalty];
}

- (void)displayBonus:(CGFloat)bonus
{
    self.gameBonus.text = [NSString stringWithFormat:@"%.2f",bonus];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"GridSegue"]){
        self.gridVC = segue.destinationViewController;
        self.gridVC.delegate = self;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
