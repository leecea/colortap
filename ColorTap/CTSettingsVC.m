//
//  CTSettingsVC.m
//  ColorTap
//
//  Created by Andy Leece on 1/27/15.
//  Copyright (c) 2015 Andy Leece. All rights reserved.
//

#import "CTSettingsVC.h"
#import "SoundHelper.h"

@interface CTSettingsVC ()

@property (weak, nonatomic) IBOutlet UISwitch *soundEffectsSwitch;

@end

@implementation CTSettingsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.soundEffectsSwitch.on = [[SoundHelper sharedInstance] soundOn];
}

- (IBAction)soundEffectsSelected:(id)sender {
    
    [[SoundHelper sharedInstance] setSoundOn:self.soundEffectsSwitch.on];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setBool:self.soundEffectsSwitch.on forKey:SOUND_EFFECTS];
    [defaults synchronize];
}

- (IBAction)mainMenuPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
