//
//  CTOpeningScreenVC.m
//  ColorTap
//
//  Created by Andy Leece on 10/9/14.
//  Copyright (c) 2014 Andy Leece. All rights reserved.
//

#import "CTOpeningScreenVC.h"
#import "CTViewController.h"
#import "SoundHelper.h"

static NSString *const HIGH_SCORE_PRACTICE = @"HighScorePractice";
static NSString *const HIGH_SCORE_FULL_GAME = @"HighScoreFullGame";
static NSString *const FULL_GAME_UNLOCKED = @"FullGameUnlocked";

@interface CTOpeningScreenVC ()

@property (weak, nonatomic) IBOutlet UILabel *unlockFullGameMessage;
@property (weak, nonatomic) IBOutlet UILabel *fullGameLabel;
@property (weak, nonatomic) IBOutlet UIButton *fullGameStart;
@property (weak, nonatomic) IBOutlet UIImageView *unlockFullGameImage;
@property (assign, nonatomic) NSInteger fullGameUnlocked;
@property (weak, nonatomic) CTViewController *mainVC;

@end

@implementation CTOpeningScreenVC

- (void)viewDidLoad {
    [super viewDidLoad];

    //self.canDisplayBannerAds = YES;
    
    [self getDefaults];
    
    if(self.fullGameUnlocked) [self unlockFullGame];
}

- (void)unlockFullGame
{
    self.fullGameLabel.enabled = YES;
    self.fullGameStart.enabled = YES;
    self.unlockFullGameMessage.hidden = YES;
    self.unlockFullGameImage.hidden = YES;
}

#pragma mark - Manage defaults

- (void)getDefaults
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    self.highScorePractice = [defaults integerForKey:HIGH_SCORE_PRACTICE];
    self.highScoreFullGame = [defaults integerForKey:HIGH_SCORE_FULL_GAME];
    self.fullGameUnlocked = [defaults integerForKey:FULL_GAME_UNLOCKED];
    
    [[SoundHelper sharedInstance] setSoundOn:[defaults boolForKey:SOUND_EFFECTS]];
}

- (void)saveHighScores
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setInteger:self.highScorePractice forKey:HIGH_SCORE_PRACTICE];
    [defaults setInteger:self.highScoreFullGame forKey:HIGH_SCORE_FULL_GAME];
    [defaults synchronize];
}

- (void)saveFullGameUnlocked
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];

    self.fullGameUnlocked = 1;
    
    [defaults setInteger:self.fullGameUnlocked forKey:FULL_GAME_UNLOCKED];
    [defaults synchronize];
    
    [self unlockFullGame];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Segue Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"HelpSegue"] || [segue.identifier isEqualToString:@"SettingsSegue"]){
        
        // no action needed for these segues 
    }
    else {
        self.mainVC = segue.destinationViewController;
        self.mainVC.delegate = self;
        
        self.mainVC.playingFullGame = [segue.identifier isEqualToString:@"FullGameSegue"];
    }
}

@end
