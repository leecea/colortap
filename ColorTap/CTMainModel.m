//
//  CTMainModel.m
//  ColorTap
//
//  Created by Andy Leece on 9/21/14.
//  Copyright (c) 2014 Andy Leece. All rights reserved.
//
// ****************************************************
// MODEL FOR OVERALL VC.  HIGH-LEVEL GAME INFO
// ****************************************************

#import "CTMainModel.h"

@interface CTMainModel()

@property (nonatomic, strong) NSArray *levelData;

@end


@implementation CTMainModel

- (id)init
{
    self = [super init];
    
    return self;
}

- (void)setupFullGameLevelData
{
    // create level data for full game levels (bonus values must be -ve and penalties must be +ve)
    //
    NSDictionary *level0 = @{LEVEL_SCORE : @0,
                             LEVEL_PENALTY : @5,
                             LEVEL_BONUS : @-6,
                             LEVEL_IMAGE : [UIImage imageNamed:@"medalGold.png"],
                             LEVEL_TIME_PER_LOCK : @1.2,
                             LEVEL_BONUS_MIN : @5,
                             LEVEL_BONUS_MAX : @5,
                             LEVEL_PENALTY_DEATH : @NO,
                             LEVEL_BONUS_DEATH : @NO,
                             LEVEL_COLOR : GRID_COLOR(0)
                             };
    NSDictionary *level1 = @{LEVEL_SCORE : @0,
                             LEVEL_PENALTY : @1,
                             LEVEL_BONUS : @-6,
                             LEVEL_IMAGE : [UIImage imageNamed:@"medalGold.png"],
                             LEVEL_TIME_PER_LOCK : @1.2,
                             LEVEL_BONUS_MIN : @4,
                             LEVEL_BONUS_MAX : @4,
                             LEVEL_PENALTY_DEATH : @YES,
                             LEVEL_BONUS_DEATH : @NO,
                             LEVEL_COLOR : GRID_COLOR(1)
                             };
    NSDictionary *level2 = @{LEVEL_SCORE : @0,
                             LEVEL_PENALTY : @1,
                             LEVEL_BONUS : @-7,
                             LEVEL_IMAGE : [UIImage imageNamed:@"medalGold.png"],
                             LEVEL_TIME_PER_LOCK : @1.1,
                             LEVEL_BONUS_MIN : @4,
                             LEVEL_BONUS_MAX : @4,
                             LEVEL_PENALTY_DEATH : @YES,
                             LEVEL_BONUS_DEATH : @YES,
                             LEVEL_COLOR : GRID_COLOR(2)
                             };
    NSDictionary *level3 = @{LEVEL_SCORE : @0,
                             LEVEL_PENALTY : @1,
                             LEVEL_BONUS : @-8,
                             LEVEL_IMAGE : [UIImage imageNamed:@"medalGold.png"],
                             LEVEL_TIME_PER_LOCK : @1.1,
                             LEVEL_BONUS_MIN : @3,
                             LEVEL_BONUS_MAX : @5,
                             LEVEL_PENALTY_DEATH : @YES,
                             LEVEL_BONUS_DEATH : @YES,
                             LEVEL_COLOR : GRID_COLOR(3)
                             };
    
    self.levelData = @[level0, level1, level2, level3];
}

- (NSString *)descriptionForLevel:(NSInteger)gameLevel
{
    NSString *description;
    NSDictionary *levelData = [self dataForLevel:gameLevel];
    
    description = [NSString stringWithFormat:@"\nTime limit: %ld secs\n",[[levelData valueForKey:LEVEL_TIME_PER_LOCK] integerValue]*60];
    
    // Create description of bonus frequency
    //
    NSString *bonusDescription;
    NSInteger bonusMin = [[levelData valueForKey:LEVEL_BONUS_MIN] integerValue];
    NSInteger bonusMax = [[levelData valueForKey:LEVEL_BONUS_MAX] integerValue];
    
    if(bonusMax == bonusMin) bonusDescription = [NSString stringWithFormat:@"Bonus after %ld locks\n",(long)bonusMin];
    else bonusDescription = [NSString stringWithFormat:@"Bonus from %ld to %ld locks\n",(long)bonusMin, (long)bonusMax];
    
    // Create sudden death descriptions
    //
    NSString *suddenDeathDescription = nil;
    
    if([[levelData valueForKey:LEVEL_PENALTY_DEATH] boolValue]) suddenDeathDescription = @"Game ends on penalty";
    if([[levelData valueForKey:LEVEL_BONUS_DEATH] boolValue]){
        if(suddenDeathDescription) suddenDeathDescription = [suddenDeathDescription stringByAppendingString:@" or missed bonus"];
        else suddenDeathDescription = @"Game ends on missed bonus";
    }
    
    // Add new strings to end of description
    
    description = [description stringByAppendingString:bonusDescription];
    
    if(suddenDeathDescription) description = [description stringByAppendingString:[suddenDeathDescription stringByAppendingString:@"\n"]];
    
    return description;
}

- (void)setupPracticeLevelData
{
    // create level data for practice levels (bonus values must be -ve and penalties must be +ve)
    //
    NSDictionary *level0 = @{LEVEL_SCORE : @0,
                             LEVEL_PENALTY : @3,
                             LEVEL_BONUS : @-4,
                             LEVEL_IMAGE : [UIImage imageNamed:@"medalBronze.png"],
                             LEVEL_TIME_PER_LOCK : @1.5,
                             LEVEL_BONUS_MIN : @5,
                             LEVEL_BONUS_MAX : @5,
                             LEVEL_PENALTY_DEATH : @NO,
                             LEVEL_BONUS_DEATH : @NO,
                             LEVEL_COLOR : NO_COLOR
                             };
    NSDictionary *level1 = @{LEVEL_SCORE : @1750, //1750
                             LEVEL_PENALTY : @4,
                             LEVEL_BONUS : @-5,
                             LEVEL_IMAGE : [UIImage imageNamed:@"medalSilver.png"],
                             LEVEL_TIME_PER_LOCK : @1.3,
                             LEVEL_BONUS_MIN : @5,
                             LEVEL_BONUS_MAX : @5,
                             LEVEL_PENALTY_DEATH : @NO,
                             LEVEL_BONUS_DEATH : @NO,
                             LEVEL_COLOR : NO_COLOR
                             };
    NSDictionary *level2 = @{LEVEL_SCORE : @1900, // 2000
                             LEVEL_PENALTY : @5,
                             LEVEL_BONUS : @-6,
                             LEVEL_IMAGE : [UIImage imageNamed:@"medalGold.png"],
                             LEVEL_TIME_PER_LOCK : @1.2,
                             LEVEL_BONUS_MIN : @5,
                             LEVEL_BONUS_MAX : @5,
                             LEVEL_PENALTY_DEATH : @NO,
                             LEVEL_BONUS_DEATH : @NO,
                             LEVEL_COLOR : NO_COLOR
                             };
    
    self.levelData = @[level0, level1, level2];
}

#pragma mark - Level methods

- (NSInteger)levelForScore:(NSInteger)score
{
    NSInteger i = [self maxLevel];
    
    while(i > 0 && score < [self scoreForLevel:i]) i--;
    
    return i;
}

- (NSInteger)maxLevel
{
    return ([self.levelData count] - 1);
}

- (NSDictionary *)dataForLevel:(NSInteger)gameLevel
{
    return self.levelData[gameLevel];
}

- (NSInteger)scoreForLevel:(NSInteger)gameLevel
{
    return [[self.levelData[gameLevel] valueForKey:LEVEL_SCORE] intValue];
}


@end
