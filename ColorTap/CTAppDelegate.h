//
//  CTAppDelegate.h
//  ColorTap
//
//  Created by Andy Leece on 8/21/14.
//  Copyright (c) 2014 Andy Leece. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CTAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
