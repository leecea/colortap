//
//  CTViewController.h
//  ColorTap
//
//  Created by Andy Leece on 8/21/14.
//  Copyright (c) 2014 Andy Leece. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CTOpeningScreenVC.h"

@interface CTViewController : UIViewController 

- (void)displayGameTime:(CGFloat)time;
- (void)displayPenalty:(CGFloat)penalty;
- (void)displayBonus:(CGFloat)bonus;

- (void)displayGameOverMessageWithScore:(NSInteger)score andReason:(NSString *)reason;
- (void)fullGameLevelOverWithScore:(NSInteger)score andContinueFlag:(BOOL)continueGame andReason:(NSString *)reason;

@property (weak, nonatomic) CTOpeningScreenVC *delegate;
@property (assign, nonatomic) BOOL playingFullGame;

@end
